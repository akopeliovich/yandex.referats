﻿using System;
using System.Collections.Generic;
using System.Linq;

using Nest;

using Yandex.Referats.Services.Referats;

namespace Yandex.Referats.Services.ElasticSearch {
	public class ElasticReferatsService : IElasticReferatsService {
		private readonly IElasticClient _client;

		public ElasticReferatsService() {
			var settings = new ConnectionSettings(new Uri("http://127.0.0.1:9200"))
				.DefaultIndex("referats");
			_client = new ElasticClient(settings);
		}

		public void Index(IEnumerable<Referat> referats) {
			if (_client.GetMapping<Referat>().Mapping == null) {
				_client.Map<Referat>(m => m);
			}

			foreach (var referat in referats) {
				_client.Index(referat);
			}
		}

		public object SearchReferatsAndThemes(string query, IEnumerable<string> themes) {
			var result = _client.Search<Referat>(
				s => s
					.From(0)
					.Size(10)
					.Source(ss => ss.Includes(t => t.Field(f => f.Title)))
					.Query(t => FuzzyQuery(t, query))
				);

			var themesResult = _client.Search<Referat>(
				s => s.Source(ss => ss.Includes(t => t.Field(f => f.Themes)))
					.Query(t => FuzzyQuery(t, query))).Documents.SelectMany(d => d.Themes).GroupBy(t => t).Select(group => new {
						Theme = group.Key,
						Count = group.Count()
					});

			return new { referats = result.Documents, themes = themesResult };
		}

		private QueryContainer FuzzyQuery(QueryContainerDescriptor<Referat> t, string query) {
			return t.Fuzzy(
				c => c
					.Name("named_query")
					.Boost(1.1)
					.Field(p => p.Content)
					.Fuzziness(Fuzziness.Auto)
					.Value(query)
					.MaxExpansions(100)
					.PrefixLength(3)
					.Transpositions());
		}
	}
}