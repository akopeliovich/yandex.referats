﻿using System.Collections.Generic;

using Yandex.Referats.Services.Referats;

namespace Yandex.Referats.Services.ElasticSearch {
	public interface IElasticReferatsService {
		void Index(IEnumerable<Referat> referats);

		object SearchReferatsAndThemes(string query, IEnumerable<string> themes);
	}
}