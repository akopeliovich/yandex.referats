﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Yandex.Referats.Services.Referats {
	public class YandexReferatsService : IReferatsService {
		private readonly IEnumerable<string> _themes = new[] {
			"astronomy",
			"geology",
			"gyroscope",
			"literature",
			"marketing",
			"mathematics",
			"music",
			"polit",
			"agrobiologia",
			"law",
			"psychology",
			"geography",
			"physics",
			"philosophy",
			"chemistry",
			"estetica"
		};

		private readonly string _urlMask = "https://yandex.ru/referats/write/?t={0}";

		public async Task<IEnumerable<Referat>> GenerateReferats(int referatCount, int themeCount) {
			var referats = new List<Referat>();

			using (var client = new HttpClient()) {
				for (var i = 0; i < referatCount; i++) {
					var themes = GenerateThemes(themeCount);
					var url = GetUrl(themes);

					var result = await GetResponseResult(client, url);

					referats.Add(ParseResult(result, themes));
				}
			}

			return referats;
		}

		private Referat ParseResult(string result, IEnumerable<string> themes) {
			return new Referat {
				Title = Match(result, "<strong>(.*)<\\/strong>").Groups[1].Value,
				Themes = themes,
				Content = string.Join(" ", new Regex("<p>(.*)<\\/p>").Matches(result).OfType<Match>().Select(x => x.Groups[1].Value))
			};
		}

		private static Match Match(string result, string pattern) {
			return Regex.Match(result, pattern);
		}

		private async Task<string> GetResponseResult(HttpClient client, string url) {
			using (var response = await client.GetAsync(url)) {
				return await response
					.EnsureSuccessStatusCode()
					.Content
					.ReadAsStringAsync();
			}
		}

		private IEnumerable<string> GenerateThemes(int themeCount) {
			return _themes
				.OrderBy(t => Guid.NewGuid())
				.Take(themeCount);
		}

		private string GetUrl(IEnumerable<string> themes) {
			return string.Format(_urlMask, string.Join("+", themes));
		}
	}
}