﻿using System.Collections.Generic;

namespace Yandex.Referats.Services.Referats {
	public class Referat {
		public string Title { get; set; }

		public string Content { get; set; }

		public IEnumerable<string> Themes { get; set; } 
	}
}