﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Yandex.Referats.Services.Referats {
	public interface IReferatsService {
		Task<IEnumerable<Referat>> GenerateReferats(int referatCount = 10, int themeCount = 3);
	}
}