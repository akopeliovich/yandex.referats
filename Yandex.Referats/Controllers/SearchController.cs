﻿using System.Threading.Tasks;
using System.Web.Http;

using Yandex.Referats.Services.ElasticSearch;
using Yandex.Referats.Services.Referats;

namespace Yandex.Referats.Controllers {
	public class SearchController : ApiController {
		private readonly IReferatsService _referatsService = new YandexReferatsService();
		private readonly IElasticReferatsService _elasticReferatsService = new ElasticReferatsService();

		// GET: api/Search
		[AcceptVerbs("GET")]
		[HttpGet]
		public async Task<IHttpActionResult> Get(string query, [FromUri] string[] themes) {
			var referats = await _referatsService.GenerateReferats();
			_elasticReferatsService.Index(referats);

			return Json(_elasticReferatsService.SearchReferatsAndThemes(query, themes));
		}
	}
}